#pragma once
#include <cmath>
#include "Vector3.h"
class Vector4
{
public:
	Vector4() { x = y = z = w = 0; };
	Vector4(float x, float y, float z, float w) 
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;

	};
	~Vector4();

public:
	float operator[] (int index) const { return data[index]; }
	float& operator[] (int index) { return data[index]; }

	Vector4 operator+ (const Vector4& other) const {
		return { x + other.x, y + other.y, z + other.z, w + other.w }; // constructs objects through call to overriden constructor then return the result
	}

	Vector4 operator- (float scalar) const {
		return { x - scalar, y - scalar, z - scalar, w - scalar };
	}

	Vector4 operator* (float scalar) const {
		return { x * scalar, y * scalar, z * scalar, w * scalar };
	}

	Vector4 operator/ (float scalar) {
		return { x / scalar, y / scalar, z / scalar, w / scalar };
	}

	Vector4& operator= (const Vector4& other) {
		x = other.x; y = other.y; z = other.z; w = other.w;
		return *this;
	}

	Vector4& operator+= (const Vector4& other) {
		x += other.x; y += other.y; z += other.z; w += other.w;
		return *this;
	}

	Vector4& operator-= (const Vector4& other) {
		x -= other.x; y -= other.y; z -= other.z; w -= other.w;
		return *this;
	}

	Vector4& operator*= (const Vector4& other) {
		x *= other.x; y *= other.y; z *= other.z; w *= other.w;
		return *this;
	}

	Vector4& operator/= (const Vector4& other) {
		x /= other.x; y /= other.y; z /= other.z; w /= other.w;
		return *this;
	}

	Vector4& operator *=  (float scalar) {
		x *= scalar;
		y *= scalar;
		z *= scalar;
		w *= scalar;
		return *this;
	}

	Vector4& operator- (const Vector4& other) {
		x = x - other.x;
		y = y - other.y;
		z = z - other.z;
		w = w - other.w;
		return *this;
	}

	float magnitude() const {
		return sqrt(x * x + y * y + z * z + w * w);
	}

	void normalise() {
		float mag = sqrt(x * x + y * y + z * z + w * w);
		x /= mag;
		y /= mag;
		z /= mag;
		w /= mag;

	}

	float dot(const Vector4& other) const {
		return x * other.x + y * other.y + z * other.z + w * other.w;
	}

	Vector4 cross(const Vector4& other) const {
		return { y * other.z - z * other.y,
			z * other.x - x * other.z,
			x * other.y - y  * other.x,
			0};
	}

	Vector4& operator= (const Vector3& other) {
		x = other.x;
		y = other.y;
		z = other.z;
		w = other.w;
		return *this;
	}

	operator float* () { return &x; }
	operator const float* () const { return &x; }

public:
	union
	{
		struct {
			float x, y, z, w;
		};

		float data[4];
	};
};

Vector4 operator* (float scalar, Vector4& v);
