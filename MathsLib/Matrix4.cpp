#include "stdafx.h"
#include "Matrix4.h"


Matrix4::Matrix4()
{
}


Matrix4::~Matrix4()
{
}



const Matrix4 Matrix4::identity = Matrix4(1, 0, 0, 0,
										  0, 1, 0, 0,
										  0, 0, 1, 0,
										  0, 0, 0, 1);