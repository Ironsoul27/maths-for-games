#pragma once
#include "Vector3.h"
#include <math.h>

class Matrix3
{
public:
	Matrix3() {/* xAxis = WorldOrigin, yAxis = WorldOrigin, zAxis = WorldOrigin;*/ }

	//Matrix3() { this->xAxis = xAxis; };

	Matrix3(float x_x, float x_y, float x_z,
		float y_x, float y_y, float y_z,
		float z_x, float z_y, float z_z) {

		data[0][0] = x_x, data[0][1] = x_y, data[0][2] = x_z,
			data[1][0] = y_x, data[1][1] = y_y, data[1][2] = y_z,
			data[2][0] = z_x, data[2][1] = z_y, data[2][2] = z_z;
	};


	~Matrix3();

	Vector3& operator[] (int index) { 
		return axis[index]; 
	}
	const Vector3& operator[] (int index) const {
		return axis[index];
	}

	Matrix3& operator= (const Matrix3& other) {
		xAxis = other.xAxis;
		yAxis = other.yAxis;
		zAxis = other.zAxis;

		return *this;
	}

	Matrix3 operator* (const Matrix3& other) const {
		Matrix3 result;

		for (int r = 0; r < 3; ++r) {
			for (int c = 0; c < 3; ++c) {
				result.data[c][r] = data[0][r] * other.data[c][0] +
									data[1][r] * other.data[c][1] +
									data[2][r] * other.data[c][2];
			}
		}
		return result;
	}

	Vector3 operator* (const Vector3& v) const {

		Vector3 result;

		for (int r = 0; r < 3; ++r) {
			result[r] = data[0][r] * v[0] +
						data[1][r] * v[1] +
						data[2][r] * v[2];
		}
		return result;
	}

	Matrix3 transposed() const {
		
		Matrix3 result;

		for (int r = 0; r < 3; ++r)
			for (int c = 0; c < 3; ++c)
				result.data[r][c] = data[c][r];

		return result;
	}
	

	void setScaled(float x, float y, float z) {

		xAxis = { x, 0, 0 };
		yAxis = { 0, y, 0 };
		zAxis = { 0, 0, z };
	}

	void setScaled(const Vector3& v) {

		xAxis = { v.x, 0, 0 };
		yAxis = { 0, v.y, 0 };
		zAxis = { 0, 0, v.z };
	}
	
	void scale(float x, float y, float z) {
		Matrix3 m;
		m.setScaled(x, y, z);

		*this = *this * m;
	}

	void scale(const Vector3& v) {
		Matrix3 m;
		m.setScaled(v.x, v.y, v.z);

		*this = *this * m;
	}

	void setRotateX(float radians) {

		xAxis = { 1, 0, 0};
		yAxis = { 0, cosf(radians), sinf(radians) };
		zAxis = { 0, -sinf(radians), cosf(radians) };
	}

	void setRotateY(float radians) {

		xAxis = { cosf(radians), 0, -sinf(radians) };
		yAxis = { 0, 1, 0 };
		zAxis = { sinf(radians), 0, cosf(radians) };
	}

	void setRotateZ(float radians) {

		xAxis = { cosf(radians), sinf(radians), 0 };
		yAxis = { -sinf(radians), cosf(radians), 0 };
		zAxis = { 0, 0, 1 };
	}


	void rotateX(float radians) {
		Matrix3 m;
		m.setRotateX(radians);

		*this = *this * m;
	}

	void rotateY(float radians) {
		Matrix3 m;
		m.setRotateY(radians);

		*this = *this * m;
	}

	void rotateZ(float radians) {
		Matrix3 m;
		m.setRotateZ(radians);

		*this = *this * m;
	}
	
	void setEuler(float pitch, float yaw, float roll) {
		Matrix3 x, y, z;
		
		x.setRotateX(pitch);
		y.setRotateY(yaw);
		z.setRotateZ(roll);

		*this = z * y * x;

	}

	void translate(float x, float y) {
		translation += Vector3(x, y, 0);
	}

	operator float* () { return m; }
	operator const float* () const { return m; } //////////////////

public:
	union {
		struct {
			Vector3 xAxis;
			Vector3 yAxis;
			union {
				Vector3 zAxis;
				Vector3 translation;
			};
			//Vector3 zAxis;
		};
		Vector3 axis[3];
		float data[3][3];
		float m[9];
	};
		
	static const Matrix3 identity;
	//Vector3 WorldOrigin = { 0, 0, 0 };
};

// include in neccessary cpp files to define the identity
//const Matrix3 Matrix3::identity = Matrix3(1, 0, 0, 0, 1, 0, 0, 0, 1);

//if key was pressed down