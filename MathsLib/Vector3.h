#pragma once
#include <cmath>
class Vector3
{
public:
	Vector3() { x = y = z = 0; };
	Vector3(float x, float y, float z) 
	{
		this->x = x;
		this->y = y;
		this->z = z;
	};
	~Vector3();

public:
	float operator[] (int index) const { return data[index]; }
	float& operator[] (int index) { return data[index]; }

	Vector3 operator+ (const Vector3& other) const {
		return { x + other.x, y + other.y, z + other.z };
	}

	Vector3 operator- (float scalar) const {
		return { x - scalar, y - scalar, z - scalar };
	}

	Vector3 operator* (float scalar) const {
		return { x * scalar, y * scalar, z * scalar };
	}

	Vector3 operator/ (float scalar) const {
		return { x / scalar, y / scalar, z / scalar };
	}

	Vector3& operator= (const Vector3& other) {
		x = other.x; y = other.y; z = other.z;
		return *this;
	}

	Vector3& operator+= (const Vector3& other) {
		x += other.x; y += other.y; z += other.z;
		return *this;
	}

	Vector3& operator-= (const Vector3& other) {
		x -= other.x; y -= other.y; z -= other.z;
		return *this;
	}

	Vector3& operator*= (const Vector3& other) {
		x *= other.x; y *= other.y; z *= other.z;
		return *this;
	}

	Vector3& operator/= (const Vector3& other) {
		x /= other.x; y /= other.y; z /= other.z;
		return *this;
	}

	Vector3& operator* (const Vector3& v) {

	}

	Vector3& operator- (const Vector3& other) {
		x = x - other.x;
		y = y - other.y;
		z = z - other.z;
		return *this;
	}

	//float magnitude() const { return sqrt(x * x + y * y + z * z); } // slower but more accurate

	float magnitudeSqr() const { return (x * x + y * y + z * z); }


	float distance(const Vector3& other) const {
		float diffx = x - other.x;
		float diffy = y - other.y;
		float diffz = z - other.z;
		return sqrt(diffx * diffx + diffy * diffy + diffz * diffz);
	}

	void normalise() {
		float mag = sqrt(x * x + y * y + z * z);
		x /= mag;
		y /= mag;
		z /= mag;
	}

	Vector3 normalised() const {
		float mag = sqrt(x * x + y * y + z * z);
		return { x / mag, y / mag, z / mag };
	}

	float magnitude() const {
		return sqrt(x * x + y * y + z * z);
	}

	float dot(const Vector3& other) const {
		return x * other.x + y * other.y + z * other.z;
	}

	Vector3 cross(const Vector3& other) const {
		return { y * other.z - z * other.y,
			z * other.x - x * other.z,
			x * other.y - y * other.x 
		};
	}

	operator float* () { return &x; }
	operator const float* () const { return &x; }


	//Vector3 normalise(const Vector3& v) {    // global method
	//	float mag = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
	//	return { v.x / mag, v.y / mag, v.z / mag };
	//}

public:
	union
	{
		struct {
			float x, y;
			union {
				float z, w;
			};
		};

		float data[3];
	};
};

Vector3 operator* (float scalar, Vector3& v);