#pragma once
#include <cmath>
class Vector2
{
public:
	Vector2() { x, y = 0; };
	Vector2(float x, float y)
	{ 
		this->x = x;
		this->y = y;
	};
	~Vector2();

public:
	float operator[] (int index) const { return data[index]; }
	float& operator[] (int index) { return data[index]; }

	Vector2 operator+ (const Vector2& other) const {
		return { x + other.x, y + other.y };
		//return Vector2(x + other.x, y + other.y); // equivalant
	}

	Vector2 operator- (float scalar) const {
		return { x - scalar, y - scalar };
	}

	Vector2 operator* (float scalar) const {
		return { x * scalar, y * scalar };
	}

	Vector2 operator/ (float scalar) const {
		return { x / scalar, y / scalar };
	}

	Vector2& operator= (const Vector2& other) {
		x = other.x; y = other.y;
		return *this;
	}

	Vector2& operator+= (const Vector2& other) {
		x += other.x; y += other.y;
		return *this;
	}

	Vector2& operator-= (const Vector2& other) {
		x -= other.x; y -= other.y;
		return *this;
	}

	Vector2& operator*= (const Vector2& other) {
		x *= other.x; y *= other.y;
		return *this;
	}

	Vector2& operator/= (const Vector2& other) {
		x /= other.x; y /= other.y;
		return *this;
	}

	Vector2& operator- (const Vector2& other) {
		x = x - other.x;
		y = y - other.y;
		return *this;
	}


	float magnitudeSqr() const { return (x * x + y * y); }


	float distance(const Vector2& other) const {
		float diffx = x - other.x;
		float diffy = y - other.y;
		return sqrt(diffx * diffx + diffy * diffy);
	}

	void normalise() {
		float mag = sqrt(x * x + y * y);
		x /= mag;
		y /= mag;
	}

	Vector2 normalised() const {
		float mag = sqrt(x * x + y * y);
		return { x / mag, y / mag };
	}

	float magnitude() const {
		return sqrt(x * x + y * y);
	}

	float dot(const Vector2& other) const {
		return x * other.x + y * other.y;
	}

	Vector2 cross(const Vector2& other) const {     /////////////////
		return {
			x * other.y - y * other.x,
			y * other.x - x * other.y };

			/*y * other.z - z * other.y,
			z * other.y - y * other.x,
			x * other.y - y * other.x*/
	
	}

	operator float* () { return &x; }
	operator const float* () const { return &x; }

public:
	union
	{
		struct {
			float x, y;
		};

		float data[2];
	};	
};

Vector2 operator* (float scalar, Vector2& v);